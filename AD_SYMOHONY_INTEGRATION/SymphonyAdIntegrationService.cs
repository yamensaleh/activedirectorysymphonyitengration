﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Text;
using System.Timers;
using System.DirectoryServices;
using System.DirectoryServices.ActiveDirectory;
using System.IO;
using System.Configuration;
using System.Diagnostics;

namespace AD_SYMOHONY_INTEGRATION
{
    public class SymphonyAdIntegrationService
    {
        private readonly Timer _timer;
        public SymphonyAdIntegrationService()
        {
            _timer = new Timer(1000) { AutoReset = true };
            _timer.Elapsed += TimerElapsed;
            //GetADUsers();
        }
        private void TimerElapsed(object sender, ElapsedEventArgs e)
        {
            int h = DateTime.Now.Hour;
            int m = DateTime.Now.Minute;
            int s = DateTime.Now.Second;
            if (h== Properties.Settings.Default.h && m == Properties.Settings.Default.m && s == Properties.Settings.Default.s) {
                
                GetADUsers();
            }
        }
        public void start()
        {
            _timer.Start();
            File.AppendAllText(@"Integraiton_Log.txt", DateTime.Now.ToString() + "Service Started Successfully " + Environment.NewLine);
        }
        public void stop()
        {
            File.AppendAllText(@"Integraiton_Log.txt", DateTime.Now.ToString() + "Service Stopped Successfully " + Environment.NewLine);
            _timer.Stop();
        }
        /// <summary>  this method will connecto to the active directory and return all users depends on the search filter asigned in the app.config</summary>
        private void GetADUsers()
        {
            File.AppendAllText(@"Integraiton_Log.txt", DateTime.Now.ToString() + " Started getting Users " + Environment.NewLine);
            try { 
                AD_SymphonyDS.VAR_MAPDataTable vmdt = new AD_SymphonyDS.VAR_MAPDataTable();
                AD_SymphonyDSTableAdapters.VAR_MAPTableAdapter vmta = new AD_SymphonyDSTableAdapters.VAR_MAPTableAdapter();
                AD_SymphonyDSTableAdapters.CUSTOMADVALUESTableAdapter cvta = new AD_SymphonyDSTableAdapters.CUSTOMADVALUESTableAdapter();
                vmta.Fill(vmdt);
                StringBuilder userrecord = new StringBuilder();
                string currentUsersFile = Properties.Settings.Default.filepath; 
                string DomainPath = Properties.Settings.Default.DomainName; //testing with
                string AuthUserName = Properties.Settings.Default.AuthUserName;
                string AuthPassword = Properties.Settings.Default.AuthPassword;
                string ADSearchFilter =Properties.Settings.Default.ADSearchFilter;
                string reqField = Properties.Settings.Default.RequiredFeild;
                DirectoryEntry searchRoot = new DirectoryEntry(DomainPath);//, AuthUserName, AuthPassword);
                DirectorySearcher search = new DirectorySearcher(searchRoot);
                search.SizeLimit = 3000;
                search.Filter = ADSearchFilter;
                foreach (DataRow row in vmdt.Rows)
                {
                    search.PropertiesToLoad.Add(row["ADVARIABLE"].ToString());
                }
                SearchResult result;
                
                SearchResultCollection resultCol = search.FindAll();
                
                if (resultCol != null)
                {
                    for (int counter = 0; counter < resultCol.Count; counter++)
                    {
                    try{
                            result = resultCol[counter];
                            if ((String)result.Properties[reqField][0].ToString() != "")
                            {
                                userrecord.Append("*** DOCUMENT BOUNDARY ***" + Environment.NewLine + "FORM=LDUSER" + Environment.NewLine);
                                foreach (DataRow row in vmdt.Rows)
                                {
                                    try
                                    {
                                        if ((bool)row["ISFIXED"] == true)
                                        {
                                            userrecord.AppendLine(row["SYMVARIABLE"].ToString() + " " + row["FIXEDVALUE"].ToString());
                                        }
                                        else if ((bool)row["ISCUSTOM"] == true)
                                        {
                                            string processedvalue = getString(cvta.getCustomValue(row["ADVARIABLE"].ToString(), (String)result.Properties[row["ADVARIABLE"].ToString()][0].ToString()));
                                            userrecord.AppendLine(row["SYMVARIABLE"].ToString() + " " + processedvalue);
                                        }
                                        else
                                        {
                                            if ((bool)row["ISFIXED"] == false)
                                            {
                                                userrecord.AppendLine(row["SYMVARIABLE"].ToString() + " " + (String)result.Properties[row["ADVARIABLE"].ToString()][0].ToString());
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        File.AppendAllText(@"Error.txt", DateTime.Now.ToString() + " Warning, could be AD Variable Local error 101 " + ex.Message + Environment.NewLine);
                                    }
                                }
                                System.IO.StreamWriter file = new System.IO.StreamWriter(@currentUsersFile);
                                file.WriteLine(userrecord);
                                file.Close();
                                file = null;
                            }
                            //user=null;
                    }
                    catch (Exception ex)
                    {
                        File.AppendAllText(@"Integraiton_Log.txt", DateTime.Now.ToString() + " "  + ex.Message + Environment.NewLine);
                        File.AppendAllText(@"Error.txt", DateTime.Now.ToString() + " " + ex.Message + Environment.NewLine);
                        }                  
                }
                    File.AppendAllText(@"Integraiton_Log.txt", DateTime.Now.ToString() + " Finished getting Users " + Environment.NewLine);
                }
                cvta.Dispose();
                cvta = null;
                vmdt.Dispose();
                vmdt = null;
                userrecord.Clear();
                searchRoot.Close();
                searchRoot=null;
                search=null;
                result=null;
            }
            catch (Exception ex)
            {
                File.AppendAllText(@"Error.txt", DateTime.Now.ToString() + " Local Error 102 " + ex.Message + Environment.NewLine);
            }
        }

        /// <summary>Gets the string.</summary>
        /// <param name="o">the object to be checked if is null</param>
        /// <returns>if the object is DBnull the returned value will be null, otherwise it will return the same value</returns>
        private static string getString(object o)
        {
            if (o == DBNull.Value) return null;
            return (string)o;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Topshelf;

namespace AD_SYMOHONY_INTEGRATION
{
    public class Program
    {
        static void Main(string[] args)
        {
            var exitCode = HostFactory.Run(x =>
             {
                 x.Service<SymphonyAdIntegrationService>(s =>
                 {
                 s.ConstructUsing( adintegration => new SymphonyAdIntegrationService());
                     s.WhenStarted(adintegration => adintegration.start());
                     s.WhenStopped(adintegration => adintegration.stop());


                 });
                 x.RunAsLocalSystem();
                 x.SetServiceName("NaseejIntegration");
                 x.SetDisplayName("Naseej AD-Symphony Integration");
                 x.SetDescription("");
              });
            int exitCodeValue = (int)Convert.ChangeType(exitCode, exitCode.GetTypeCode());
            Environment.ExitCode = exitCodeValue;
            Console.ReadLine();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IntegrationSettings
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
         
        }
        void GetSymphonyVariablesToList()
        {
            IntegrationDS.SymphonyUserRecordFormatDataTable SDT = new IntegrationDS.SymphonyUserRecordFormatDataTable();
            IntegrationDSTableAdapters.SymphonyUserRecordFormatTableAdapter SDA = new IntegrationDSTableAdapters.SymphonyUserRecordFormatTableAdapter();
            SDA.Fill(SDT);
            for (int i = 0; i< SDT.Count; i++)
            {
                ListViewItem li = new ListViewItem();
                li.Text = SDT[i].DCODE.ToString();
                li.SubItems.Add( SDT[i].DDESCRIPTION.ToString());
                listView1.Items.Add(li);
            }
        }
        void GetLDAPVariablesToList()
        {
            IntegrationDS.ADCODESDataTable ADT = new IntegrationDS.ADCODESDataTable();
            IntegrationDSTableAdapters.ADCODESTableAdapter ADA = new IntegrationDSTableAdapters.ADCODESTableAdapter();
            ADA.Fill(ADT);
            for (int i = 0; i < ADT.Count; i++)
            {
                ListViewItem li = new ListViewItem();
                li.Text = ADT[i].ADCODE.ToString();
                li.SubItems.Add(ADT[i].ACODEDESCRIPTION.ToString());
                listView2.Items.Add(li);
            }
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'integrationDS.CUSTOMADVALUES' table. You can move, or remove it, as needed.
            //this.cUSTOMADVALUESTableAdapter.Fill(this.integrationDS.CUSTOMADVALUES);
            // TODO: This line of code loads data into the 'integrationDS.CUSTOMADVALUES' table. You can move, or remove it, as needed.

            label2.Text= "Service is: " + GetWindowsServiceStatus("AnyDesk");
            Blink(Color.Green);
            GetSymphonyVariablesToList();
            GetLDAPVariablesToList();
            IntegrationDSTableAdapters.CUSTOMADVALUESTableAdapter customadvaluesTableAdapter1 = new IntegrationDSTableAdapters.CUSTOMADVALUESTableAdapter();
            IntegrationDS.CUSTOMADVALUESDataTable dt = new IntegrationDS.CUSTOMADVALUESDataTable();
            customadvaluesTableAdapter1.Fill(dt);
            for (int i = 0; i < dt.Rows.Count - 1; i++)
            {
                dataGridView1.Rows.Add(dt[i].ADCODE.ToString(), dt[i].ADVALUE.ToString(), dt[i].SYMPHONYVALUE.ToString());
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (checkBox1.Checked)
                {
                    listBox1.Items.Add(listView1.SelectedItems[0].Text + ":" + listView2.SelectedItems[0].Text + "!");
                    listView1.SelectedItems[0].BackColor = listView2.SelectedItems[0].BackColor = Color.DarkGray;
                }
                else
                {
                    listBox1.Items.Add(listView1.SelectedItems[0].Text + ":" + listView2.SelectedItems[0].Text);
                    listView1.SelectedItems[0].BackColor = listView2.SelectedItems[0].BackColor = Color.DarkGray;
                }
            }
            catch {
                MessageBox.Show("Select Symphony Attribute and LDAP Attribute to add it to configuration");
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (File.Exists("C:/Users/ysaleh/Documents/Source/Repos/ActiveDirectorySymphonyItengration/AD_SYMOHONY_INTEGRATION/ADFormat.cfg")) {
              if (MessageBox.Show("Delete Current Config and replace it with the new one?","Confirmation!",MessageBoxButtons.YesNo) == DialogResult.Yes){
                    File.Delete("C:/Users/ysaleh/Documents/Source/Repos/ActiveDirectorySymphonyItengration/AD_SYMOHONY_INTEGRATION/ADFormat.cfg");
                    for (int i = 0; i < listBox1.Items.Count; i++)
                    {
                        File.AppendAllText(@"C:/Users/ysaleh/Documents/Source/Repos/ActiveDirectorySymphonyItengration/AD_SYMOHONY_INTEGRATION/ADFormat.cfg", listBox1.Items[i].ToString() + Environment.NewLine);
                    }
                }
            }
            IntegrationDSTableAdapters.CUSTOMADVALUESTableAdapter customadvaluesTableAdapter1 = new IntegrationDSTableAdapters.CUSTOMADVALUESTableAdapter();
            for (int i = 0; i< dataGridView1.RowCount-1;i++)
            {
                 customadvaluesTableAdapter1.InsertQuery(dataGridView1[0,i].Value.ToString(), dataGridView1[1, i].Value.ToString(), dataGridView1[2, i].Value.ToString());
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            listBox1.Items.Add(".USER_ADDR1_BEGIN.");
        }

        private void button5_Click(object sender, EventArgs e)
        {
            listBox1.Items.Add(".USER_ADDR1_END.");
        }

        private void button7_Click(object sender, EventArgs e)
        {
            listBox1.Items.Add(".USER_XINFO_BEGIN");
        }

        private void button6_Click(object sender, EventArgs e)
        {
            listBox1.Items.Add(".USER_XINFO_END.");
        }

        private void listView2_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void button8_Click(object sender, EventArgs e)
        {
            listBox1.Items.RemoveAt(listBox1.SelectedIndex);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            string[] lines = System.IO.File.ReadAllLines("C:/Users/ysaleh/Documents/Source/Repos/ActiveDirectorySymphonyItengration/AD_SYMOHONY_INTEGRATION/ADFormat.cfg");
            foreach (string line in lines)
            {
                listBox1.Items.Add(line);
            }
            IntegrationDSTableAdapters.CUSTOMADVALUESTableAdapter customadvaluesTableAdapter1 = new IntegrationDSTableAdapters.CUSTOMADVALUESTableAdapter();
            IntegrationDS.CUSTOMADVALUESDataTable dt = new IntegrationDS.CUSTOMADVALUESDataTable();
            customadvaluesTableAdapter1.Fill(dt);
            dataGridView1.Rows.Clear();
            for (int i = 0; i < dt.Rows.Count - 1; i++)
            {
                dataGridView1.Rows.Add(dt[i].ADCODE.ToString(), dt[i].ADVALUE.ToString(), dt[i].SYMPHONYVALUE.ToString());
            }
        }

        public static String GetWindowsServiceStatus(String SERVICENAME)
        {

            ServiceController sc = new ServiceController(SERVICENAME);

            switch (sc.Status)
            {
                case ServiceControllerStatus.Running:
                    return "Running";
                case ServiceControllerStatus.Stopped:
                    return "Stopped";
                case ServiceControllerStatus.Paused:
                    return "Paused";
                case ServiceControllerStatus.StopPending:
                    return "Stopping";
                case ServiceControllerStatus.StartPending:
                    return "Starting";
                default:
                    return "Status Changing";
            }
        }
        private async void Blink(Color color)
        {
            while (true)
            {
                await Task.Delay(500);
                label2.ForeColor = label2.ForeColor == Color.Black ? color : Color.Black;
            }
        }
        public static void StartService(string serviceName, int timeoutMilliseconds)
        {
            ServiceController service = new ServiceController(serviceName);
            try
            {
                TimeSpan timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds);

                service.Start();
                service.WaitForStatus(ServiceControllerStatus.Running, timeout);
            }
            catch (Exception ex)
            {
                // ...
                MessageBox.Show(ex.Message);
            }
        }
        public static void StopService(string serviceName, int timeoutMilliseconds)
        {
            ServiceController service = new ServiceController(serviceName);
            try
            {
                TimeSpan timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds);

                service.Stop();
                service.WaitForStatus(ServiceControllerStatus.Stopped, timeout);
            }
            catch (Exception ex)
            {
                // ...
                MessageBox.Show(ex.Message);
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            StartService("AnyDesk", 1000);
            label2.Text = "Service is: " + GetWindowsServiceStatus("AnyDesk");
            Blink(Color.Green);
        }

        private void button11_Click(object sender, EventArgs e)
        {
            StopService("AnyDesk", 1000);
            label2.Text = "Service is: " + GetWindowsServiceStatus("AnyDesk");
            Blink(Color.Red);
        }

        private void listBox1_DoubleClick(object sender, EventArgs e)
        {
            if (listBox1.SelectedItem.ToString().EndsWith("!") == false)
            {
                dataGridView1.Enabled = false;
                MessageBox.Show("this is not a custom feild");
            }
            else
            { 
                dataGridView1.Enabled = true;
               
            }
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0 )
            {
                string St = listBox1.SelectedItem.ToString();
                int pFrom = St.IndexOf(":") + 1;
                int pTo = St.LastIndexOf("!");

                String result = St.Substring(pFrom, pTo - pFrom);

                dataGridView1[e.ColumnIndex, e.RowIndex].Value = result;
            }
        }

        private void cUSTOMADVALUESBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            //this.Validate();
            //this.cUSTOMADVALUESBindingSource.EndEdit();
            //this.tableAdapterManager.UpdateAll(this.integrationDS);

        }
    }
}
